import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerdetailComponent } from './powerdetail.component';

describe('PowerdetailComponent', () => {
  let component: PowerdetailComponent;
  let fixture: ComponentFixture<PowerdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PowerdetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PowerdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
