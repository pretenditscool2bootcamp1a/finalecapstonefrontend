import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hero';
  display = "none";
  backgroundvid: string = "../assets/backgroundvid.mp4";
  isMuted: string = " ";
  ourAudio: string = "../assets/audio1.mp3";

}
