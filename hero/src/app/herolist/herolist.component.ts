import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import * as Hammer from 'hammerjs';

import { Powers } from '../models/powers.model';
import { PowerService } from '../providers/power.service';
import { Heroes } from '../models/heroes.model';
import { HeroService } from '../providers/hero.service';

@Component({
  selector: 'app-hero-list',
  templateUrl: './herolist.component.html',
  styleUrls: ['./herolist.component.css']
})
export class HerolistComponent implements OnInit {
  heroes?: Heroes[];
  powers?: Powers[] = [];
  selectedHero: Heroes | null = null;
  swipeOffset = 0;
  searchPower: string = '';
  isLoading = false;

  constructor(private heroService: HeroService,
    private powerService: PowerService,
    private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.heroService.getHeroes().subscribe(heroes => {
      this.heroes = heroes;
    });
    this.isLoading = false;
  }

  getHeroes(): void {
    this.isLoading = true;
    this.heroService.getHeroes()
      .subscribe(heroes => {
        if (heroes) {
          this.heroes = heroes;
        }
      });
    this.isLoading = false;
  }

  getPowersList(hero: Heroes): string {
    return hero.superpowers?.map(sp => sp.name).join(', ') || '';
  }


  onSelect(hero: Heroes) {
    if (this.selectedHero === hero) {
      this.selectedHero = null;
    } else {
      this.selectedHero = hero;
    }
  }

  getHeroesByPower() {
    if (!this.searchPower) {
      this.getHeroesByPower();
      return;
    }
    this.heroService.searchHeroes(this.searchPower)
      .subscribe(data => {
        this.heroes = data;
      });
  }




  goToHero(id: number): void {
    this.router.navigate(['/hero', id]);
  }


  onEdit(hero: Heroes) {
    this.router.navigate(['/edit-hero', hero.superheroId]);
  }

  clearSearchPower(): void {
    this.searchPower = '';
    this.getHeroes();
  }

  onDelete(hero: Heroes) {
    if (confirm('Are you sure you want to delete this hero?')) {
      this.heroService.deleteHero(hero.superheroId).subscribe(() => {
        if (this.heroes) {
          this.heroes = this.heroes.filter(h => h.superheroId !== hero.superheroId);
        }
      });
    }
  }
}
