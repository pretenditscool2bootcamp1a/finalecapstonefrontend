import { Component, HostListener, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Heroes } from '../models/heroes.model';
import { saveAs } from 'file-saver';
import { map } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare global {
  interface Window {
    showSaveFilePicker: any;
  }
}

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.component.html',
  styleUrls: ['./image-modal.component.css']
})
export class ImageModalComponent implements OnInit {
  // @Input() images?: string[]; //= [];
  @Input() hero?: Heroes; // | null = null;
  @Input() imageGenerationResponse: any;

  selectedImage: string | null = null;
  allImagesLoaded = false;


  constructor(@Inject(MAT_DIALOG_DATA) public images: string[],
    public dialogRef: MatDialogRef<ImageModalComponent>) {
    console.log("in image-modal constructor");
    this.allImagesLoaded = false;

    if (Array.isArray(this.images)) {
      const imageLoadPromises = this.images.map((src) => new Promise((resolve) => {
        const img = new Image();
        img.onload = () => resolve(null);
        img.src = src;
      }));
      Promise.all(imageLoadPromises).then(() => {
        this.allImagesLoaded = true;
      });
    }
  }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    if ((event.target as HTMLElement).classList.contains('image-modal-container')) {
      this.onCancelClick();
    }
  }
  

  ngOnInit(): void {
   // console.log("in ngoninit of image-modal");
  }

  async onImageDoubleClick(): Promise<void> {
    console.log("in this onImageDoubleClick :");
  
    const imageName = "assets_hero_temp1.png";
    if (this.selectedImage != null ) {
      console.log("this.selectedimage is not null");
  
      const headers = new Headers({
        'User-Agent': 'PostmanRuntime/7.32.2',
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Host': 'oaidalleapiprodscus.blob.core.windows.net'
      });
  
      const response = await fetch(this.selectedImage, { headers }) as Response;
      const blob = await response.blob();
      saveAs(blob, imageName);
    }
  
    this.dialogRef.close();
  }
  
  onCancelClick(): void {
    this.dialogRef.close();
  }
}
