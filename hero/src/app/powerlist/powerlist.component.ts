import { Component, OnInit } from '@angular/core';
import { Powers } from '../models/powers.model';
import { PowerService } from '../providers/power.service';

@Component({
  selector: 'app-power-list',
  templateUrl: './powerlist.component.html',
  styleUrls: ['./powerlist.component.css']
})
export class PowerlistComponent implements OnInit {
  powers?: Powers[];

  constructor(private powerService: PowerService) {}

  ngOnInit(): void {
    this.getPowers();
  }

  getPowers(): void {
    this.powerService.getPowers()
      .subscribe(powers => this.powers = powers);
  }
}
