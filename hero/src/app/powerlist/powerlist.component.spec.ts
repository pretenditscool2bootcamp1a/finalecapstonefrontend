import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerlistComponent } from './powerlist.component';

describe('PowerlistComponent', () => {
  let component: PowerlistComponent;
  let fixture: ComponentFixture<PowerlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PowerlistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PowerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
