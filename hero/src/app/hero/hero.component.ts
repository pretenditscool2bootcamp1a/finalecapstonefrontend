import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Heroes } from '../models/heroes.model';
import { Powers } from '../models/powers.model';
import { HeroService } from '../providers/hero.service';
import { PowerService } from '../providers/power.service';
import { ImageService } from '../providers/image.service';
import { ImageModalComponent } from '../image-modal/image-modal.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { Observable, firstValueFrom } from 'rxjs';
import { ImageResponse, ImageMine } from '../interfaces/image-response.interface';



@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  hero = new Heroes(0, 'Default', 'Default Default', 'Asexual', new Date('01/01/1900'), 'Some city', 'Some other city', 'Generic', 'Everything', '../assets/hero/Default.png', 'Young-ish', []);
  superpowers?: Powers[];
  id: string = "";
  isNewHero = false;
  isEditing: boolean = false;
  availablePowers?: Powers[];
  isDevelopment = true;
  images?: string[];
  generateImagesClicked = false;
  imagesGenerating = false;


  // have to add in properties for each of the heroes properties in order to allow the ngmodel stuff to work for new or existing heroes 
  heroName: string = 'Default';
  heroSecretIdentity: string = '';
  heroGender: string = '';
  heroDateOfBirth: Date = new Date('01/01/1900');
  heroHomeLocation: string = '';
  heroCurrentLocation: string = '';
  heroCostumeDescription: string = '';
  heroWeakness: string = '';
  heroImagePath: string = '';
  heroInclusiveAge: string = '';


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private heroService: HeroService,
    private powerService: PowerService,
    private imageService: ImageService,
    private modalService: NgbModal,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    // console.log("this is what i'm getting for id", id); // Changed this.id to id
    if (id != 'new') { // Check for new
      // console.log("we're in the if (id) where supposedly we've been given a valid id ngOnInit with isediting {$1} and isnewhero {$2}", this.isEditing, this.isNewHero);
      this.getHero(Number(id));
    } else {
      // set isNewHero to true and initialize hero and superpowers
      this.isNewHero = true;
      this.isEditing = true;

      this.hero = new Heroes(0, '', '', '', new Date(), '', '', '', '', '', '', []);
      this.heroName = '';
      this.heroSecretIdentity = '';
      this.heroGender = '';
      const currentDate: Date = new Date();
      this.heroDateOfBirth = new Date('01/01/1900');
      this.heroHomeLocation = '';
      this.heroCurrentLocation = '';
      this.heroCostumeDescription = '';
      this.heroWeakness = '';
      this.heroImagePath = '../assets/hero/' + this.heroName + ".png";
      this.heroInclusiveAge = '';
      this.superpowers = [];
      console.log("we're in the else new of ngOnInit isediting {$1} isnewhero {$2} dateofbrith {$3} imagepath {$4}" + this.isEditing + this.isNewHero + this.hero.dateOfBirth + this.hero.imagePath);
    }
    // Get all powers
    this.getAllPowers();
  }



  getAllPowers(): void {
    this.powerService.getPowers().subscribe(powers => this.availablePowers = powers);
  }

  getHero(id?: number): void {
    if (id) {
      this.heroService.getHero(id)
        .subscribe(hero => {
          this.hero = hero;
          this.superpowers = hero.superpowers;

          // Move the hero properties initialization here
          this.heroName = this.hero.name;
          this.heroSecretIdentity = this.hero.secretIdentity;
          this.heroGender = this.hero.gender;
          this.heroDateOfBirth = this.hero.dateOfBirth;
          this.heroHomeLocation = this.hero.homeLocation;
          this.heroCurrentLocation = this.hero.currentLocation;
          this.heroCostumeDescription = this.hero.costumeDescription;
          this.heroWeakness = this.hero.weakness;
          this.heroImagePath = this.hero.imagePath;
          this.heroInclusiveAge = this.hero.inclusiveAge;
        });
    }
  }

  save(): void {
    console.log("In save method");
    if (this.hero) {
      this.heroService.updateHero(this.hero)
        .subscribe(() => this.goBack());
    }
  }

  onSubmit() {
    console.log("In onSubmit method");
    this.heroImagePath = '../assets/hero/' + this.heroName + ".png";
    this.hero.name = this.heroName;
    this.hero.secretIdentity = this.heroSecretIdentity;
    this.hero.gender = this.heroGender;
    this.hero.dateOfBirth = this.heroDateOfBirth;
    this.hero.homeLocation = this.heroHomeLocation;
    this.hero.currentLocation = this.heroCurrentLocation;
    this.hero.costumeDescription = this.heroCostumeDescription;
    this.hero.weakness = this.heroWeakness;
    this.hero.imagePath = this.heroImagePath;
    this.hero.inclusiveAge = this.heroInclusiveAge;
    this.hero.imagePath = this.heroImagePath;

    //    if (this.isEditing) {
    if (this.isNewHero) {
      this.hero.superpowers = this.superpowers;

      // Create a new hero
      this.heroService.addHero(this.hero).subscribe(() => {
        this.router.navigate(['/heroes-list']);
      });

    } else {
      if (this.hero) {
        this.hero.superpowers = this.superpowers;
        console.log("Updating existing superhero");

        this.heroService.updateHero(this.hero)
          .subscribe(hero => {
            console.log('hero updated: ', this.hero);
            this.isEditing = false;
            //  this.router.navigate(['/heroes-list']);
          });
      }
    }
    //    } // if this.isEditing 
  }


  onAddExistingPower(powerId: number): void {
    if (powerId) {
      this.powerService.getPower(powerId).subscribe(power => {
        if (this.superpowers) {
          this.superpowers.push(power);
        }
      });
    }
  }

  // Add this function to your hero.component.ts
  onPowerSelectChange(event: Event): void {
    const target = event.target as HTMLSelectElement;
    const powerId = parseInt(target.value, 10);
    this.onAddExistingPower(powerId);
  }


  onAddNewPower(newPowerName: string, powerDescription: string, newPowercategory: string): void {
    if (newPowerName) {
      const newPower: Powers = {
        superpowerId: 0,
        name: newPowerName,
        description: powerDescription,
        category: newPowercategory
      };
      this.powerService.addPower(newPower).subscribe(power => {
        if (this.superpowers) {
          this.superpowers.push(power);
        }
      });
    }
  }

  onAddPower(): void {
    if (!this.superpowers) {
      this.superpowers = [];
    }
    this.superpowers.push({
      superpowerId: 0,
      name: '',
      description: '',
      category: '',
    });
  }

  onDeletePower(index: number): void {
    if (this.superpowers) {
      this.superpowers.splice(index, 1);
    }
  }

  onDelete() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      if (confirm('Are you sure you want to delete this hero?')) {
        this.heroService.deleteHero(+id)
          .subscribe(() => {
            console.log('hero deleted: ', this.hero);
            this.router.navigate(['/heroes-list']);
          });
      }
    } else {
      console.error('Error: Hero ID not found');
    }
  }


  onEdit() {
    this.isEditing = !this.isEditing;
  }

  onCancel() {
    this.isEditing = false;

  }

  goBack(): void {
    window.history.back();
  }

  async onGenerateImage(): Promise<void> {
    console.log("in onGenerateImage method before calling the service");
    const prompt = this.generatePrompt();
    const n = 4;
    const size = '1024x1024';
    console.log("prompt = " + prompt);
    this.imagesGenerating = true ;
    // Call the image generation service
    // const response = await firstValueFrom(this.imageService.generateImage(prompt, n, size));
    const response: ImageResponse = await firstValueFrom(this.imageService.generateImage(prompt, n, size));

    // Extract the image URLs from the response data
    //const images = response.data.map((imageData: ImageData) => imageData.url);
    const images = response.data.map((imageData: ImageData) => (imageData as ImageMine).url);

    for (let i = 0; i < images.length; i++) {
      const imageUrl = images[i];
      const imageName = `../assets/hero/${this.hero?.name}-${i}.png`;
      console.log("about to try to save the image on the angular side with imagename:" + imageName);
      this.imageService.saveImageToLocal( imageName, imageUrl);
    }

    this.imagesGenerating = false ;
    //   const images = response.data.map((imageData: ImageMine) => imageData.url);

    // Open the image modal and pass the image URLs
    const dialogRef = this.dialog.open(ImageModalComponent, {
      data: images,
    });

    // Wait for the user to select an image or close the modal
    const result = await dialogRef.afterClosed().toPromise();

    if (result && result.image) {
      // Save the selected image to the server
      const imageName = `${this.hero?.name}.png`;
      await firstValueFrom(this.imageService.saveImage(result.image, imageName));
  
      // Update the hero's image path with a timestamp query parameter to force reload
      this.hero.imagePath = `../assets/hero/${imageName}?t=${Date.now()}`;
    } else {
      // If no image was selected, still update the hero's image path with a timestamp to force reload
      this.hero.imagePath = `../assets/hero/${this.hero?.name}.png?t=${Date.now()}`;
    }
  }



  generatePrompt(): string {
    let powerNames = "";
    if (this.hero.superpowers && this.hero.superpowers.length > 0) {
      powerNames = this.hero.superpowers.map(power => power.name).join(", ");
    } else {
      powerNames = "no powers";
    }

    const prompt = `Create an image of ${this.hero.name}, an adult ${this.hero.gender} superhero from ${this.hero.homeLocation}. Include ${this.hero.currentLocation} in the background, and showcase their powers, which include ${powerNames}. Make sure to feature their ${this.hero.costumeDescription} costume with ${this.hero.weakness}. The shot should be medium with a deep depth of view. Use a photorealism style.`;
    return prompt;
  }


  openDialog() {
    const dialogRef = this.dialog.open(ImageModalComponent, {
      data: {
        hero: this.hero,
        prompt: this.generatePrompt(),
      }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Update the hero's imagePath property to match the new image name + path
        this.hero.imagePath = `../assets/hero/${result}`;
        // Reload the hero's image from the server
        this.reloadHeroImage();
      }
    });
  }
  
  reloadHeroImage() {
    // Add a timestamp query parameter to the image URL to force the browser to reload the image
    const timestamp = new Date().getTime();
    this.hero.imagePath = `${this.hero.imagePath}?t=${timestamp}`;
  }
  
  

}
