import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { MatDialogModule } from '@angular/material/dialog'; // add this line
import {MatDialogModule, MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
// import { HammerModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
// import * as Hammer from 'hammerjs';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { HerolistComponent } from './herolist/herolist.component';
import { PowerlistComponent } from './powerlist/powerlist.component';
import { PowerdetailComponent } from './powerdetail/powerdetail.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ImagelistComponent } from './imagelist/imagelist.component';
import { HeroComponent } from './hero/hero.component';
import { EditPowerComponent } from './edit-power/edit-power.component';
import { DateFormatInterceptor } from './date-format.interceptor';
import { ImageModalComponent } from './image-modal/image-modal.component';




const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "heroes-list", component: HerolistComponent},
  { path: 'hero/:id', component: HeroComponent },
  { path: 'hero/new', component: HeroComponent },
  { path: "powers-list", component: PowerlistComponent},
  { path: "edit-power", component: PowerlistComponent},
  { path: "power", component: PowerdetailComponent},
  { path: "about-us", component: EditPowerComponent},
  { path: "image-modal", component: ImageModalComponent}
];

const MaterialComponents = [
  MatProgressSpinnerModule
]
// export class MyHammerConfig extends HammerGestureConfig {
//   override overrides = <any>{
//     swipe: {
//       direction: Hammer.DIRECTION_HORIZONTAL,
//       velocity: 0.1, // The higher the value, the faster the swipe
//       threshold: 10, // The minimum swipe distance required to trigger the gesture
//       maxPointers: 1, // The maximum number of pointers that can trigger the gesture
//       domEvents: true,
//       enable: true,
//       disable: false,
//       inputClass: Hammer.TouchInput,
//       inputTarget: null,
//       event: 'swipe',
//       recognizer: [
//         [Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL }]
//       ],
//       touchAction: 'auto',
//       waitFor: null,
//       tap: false,
//       doubletap: false,
//       hold: false,
//       transform: null,
//       recognizeWith: null,
//       requireFailure: null
//     },
//     pan: {
//       direction: Hammer.DIRECTION_HORIZONTAL,
//       threshold: 0,
//       pointers: 1
//     }
//   };
// }

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HerolistComponent,
    PowerlistComponent,
    PowerdetailComponent,
    AboutusComponent,
    ImagelistComponent,
    HeroComponent,
    ImageModalComponent
   
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes), 
    FormsModule, 
    HttpClientModule,
    NgbModalModule,
    // HammerModule,
    BrowserAnimationsModule,
    MatDialogModule, // add this line
    MatProgressSpinnerModule,
  ],
  providers: [
    // { provide: HAMMER_GESTURE_CONFIG, useValue: new MyHammerConfig() },
    { provide: HTTP_INTERCEPTORS, useClass: DateFormatInterceptor, multi: true },
    { provide: MAT_DIALOG_DATA, useValue: {} }, { provide: MatDialogRef, useValue: {} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


