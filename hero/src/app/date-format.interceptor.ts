import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DateFormatInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && event.body) {
          this.convertDate(event.body);
        }
        return event;
      })
    );
  }

  private convertDate(object: any): void {
    if (!object) return;
    
    for (const key of Object.keys(object)) {
      const value = object[key];
      if (value instanceof Date || (typeof value === 'string' && value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/))) {
        object[key] = new Date(value).toISOString().split('T')[0];
      } else if (typeof value === 'object') {
        this.convertDate(value);
      }
    }
  }
}
