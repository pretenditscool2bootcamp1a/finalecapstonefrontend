import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Powers } from '../models/powers.model';

@Injectable({
  providedIn: 'root'
})
export class PowerService {
  private powersUrl = 'https://localhost:7255/api/superpowersBackend';

  constructor(private http: HttpClient) { }

  getPowers(): Observable<Powers[]> {
    return this.http.get<Powers[]>(this.powersUrl);
  }

  getPower(id: number): Observable<Powers> {
    const url = `${this.powersUrl}/${id}`;
    return this.http.get<Powers>(url);
  }

  addPower(power: Powers): Observable<Powers> {
    return this.http.post<Powers>(this.powersUrl, power);
  }

  updatePower(power: Powers): Observable<any> {
    const url = `${this.powersUrl}/${power.superpowerId}`;
    return this.http.put(url, power);
  }

  deletePower(power: Powers | number): Observable<Powers> {
    const id = typeof power === 'number' ? power : power.superpowerId;
    const url = `${this.powersUrl}/${id}`;
    return this.http.delete<Powers>(url);
  }
}
