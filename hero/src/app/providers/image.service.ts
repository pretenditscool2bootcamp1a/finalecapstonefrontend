import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, switchMap, from } from 'rxjs';
import { ImageResponse } from '../interfaces/image-response.interface';
import { map,  tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  apiUrl = 'https://api.openai.com/v1/images/generations';
  apiKey = 'Bearer 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'; // replace this with your own API key

  constructor(private http: HttpClient) { }

  // generateImage(prompt: string, n: number, size: string): Observable<ImageResponse> {
  //   const body = { prompt, n, size };
  //   const headers = new HttpHeaders().set('Authorization', `${this.apiKey}`);

  //   return this.http.post<ImageResponse>(this.apiUrl, body, { headers });
  // }
  generateImage(prompt: string, n: number, size: string): Observable<ImageResponse> {
    const body = { prompt, n, size };
    const headers = new HttpHeaders().set('Authorization', `${this.apiKey}`);
    return this.http.post<ImageResponse>(this.apiUrl, body, { headers });
}

saveImageToLocal(imageUrl: string, imageName: string): Observable<any> {
  console.log ("in saveimagetolocal method of imageservice " + imageName); 
  const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'image/png' })
  };
 // const imageSaveUrl = `assets/heroes/${imageName}`;
 console.log ("imageservice saving url: " + imageUrl + " to filename :" + imageName) ;
  return this.http.get(imageUrl, { responseType: 'blob' }).pipe(
    switchMap((blob: Blob | undefined) => {
      return this.http.post(imageName, blob as Blob, httpOptions);
    })
  );
}

  saveImage(imageUrl: string, heroName: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'image/png' })
    };
    const imageName = `${heroName}.png`;
    const imageSaveUrl = `assets/heroes/${imageName}`;
    return this.http.get(imageUrl, { responseType: 'blob' }).pipe(
      switchMap((blob: Blob) => {
        return this.http.post(imageSaveUrl, blob, httpOptions);
      })
    );
  }
}
