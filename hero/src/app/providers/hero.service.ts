import { EventEmitter,Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


import { Heroes } from '../models/heroes.model';
import { Powers } from '../models/powers.model';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private heroesUrl = 'https://localhost:7255/api/Superheroes';

  constructor(private http: HttpClient) { }

  getHeroes(): Observable<Heroes[]> {
    return this.http.get<Heroes[]>(this.heroesUrl).pipe(
      map((heroes: Heroes[]) => {
        return heroes.map((hero: Heroes) => {
          // Map the hero object to include its powers
          hero.superpowers = hero.superpowers || [];
          return hero;
        });
      }),
      catchError(this.handleError<Heroes[]>('getHeroes', []))
    );
  }

  getHero(id: number): Observable<Heroes> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Heroes>(url).pipe(
      catchError(this.handleError<Heroes>(`getHero id=${id}`))
    );
  }
  
   addHero(hero: Heroes): Observable<Heroes> {
    return this.http.post<Heroes>(this.heroesUrl, hero);
    
  }

  updateHero(hero: Heroes): Observable<Heroes> {
    const url = `${this.heroesUrl}/${hero.superheroId}`;
    return this.http.put(url, hero).pipe(
      map((updatedHero: any) => updatedHero as Heroes)
    );
  }
  

  deleteHero(hero: Heroes | number): Observable<Heroes> {
    const id = typeof hero === 'number' ? hero : hero.superheroId;
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete<Heroes>(url);
  }

  searchHeroes(superpower: string): Observable<Heroes[]> {
    const url = `${this.heroesUrl}/search?superpower=${superpower}`;
    return this.http.get<Heroes[]>(url).pipe(
      catchError(this.handleError<Heroes[]>('searchHeroes', []))
    );
  }
  

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
