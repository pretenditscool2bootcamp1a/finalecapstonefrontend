export class Powers {
    constructor(
      public superpowerId: number,
      public name: string,
      public description: string,
      public category: string
    ) {}
  }
