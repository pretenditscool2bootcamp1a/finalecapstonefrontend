import { Powers } from './powers.model';

export class Heroes {
  constructor(
    public superheroId: number,
    public name: string,
    public gender: string,
    public secretIdentity: string,
    public dateOfBirth: Date,
    public homeLocation: string,
    public currentLocation: string,
    public costumeDescription: string,
    public weakness: string,
    public imagePath: string,
    public inclusiveAge: string,
    public superpowers?: Powers[] 
  ) {}
}
