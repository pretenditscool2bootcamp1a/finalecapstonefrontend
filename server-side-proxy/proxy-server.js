const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

const API_ENDPOINT = 'https://oaidalleapiprodscus.blob.core.windows.net';
const API_ROUTE = '/private';

app.use(
  API_ROUTE,
  createProxyMiddleware({
    target: API_ENDPOINT,
    changeOrigin: true,
    onProxyRes: function(proxyRes, req, res) {
      proxyRes.headers['Access-Control-Allow-Origin'] = '*';
    },
    pathRewrite: {
      [`^${API_ROUTE}`]: '',
    },
    ssl: {
      key: 'path/to/private/key.pem',
      cert: 'path/to/certificate.pem'
    }
  })
);

app.listen(3000, () => {
  console.log(`Proxy server listening on port 3000`);
});
